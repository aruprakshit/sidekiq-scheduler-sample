App.periodic = App.cable.subscriptions.create("PeriodicChannel",{
  connected: function () {
    // Called when the subscription is ready for use on the server
    console.log('connected');
    this.perform('speak', {
      message: "Hello World!"
    });
  },
  disconnected: function () {
    // Called when the subscription has been terminated by the server
  },
  received: function (data) {
    console.log(data);
  },
  update_count: function (argument) {
    console.log(argument);
  }
});