class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_token

  def set_token
    ActionCable.server.config.mount_path = "/cable/#{current_user.auth_token}"
  end
end
