class PeriodicChannel < ApplicationCable::Channel
  def subscribed
    stream_from "periodic_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak data
    ActionCable.server.broadcast "#{self.class.channel_name}_channel", message: data['message']
  end
end
